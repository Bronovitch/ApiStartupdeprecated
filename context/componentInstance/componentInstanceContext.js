import ComponentInstance from '../../models/componentInstance/componentInstance';

import {
    ApiServerError,
    ApiUnauthorizedError,
    ApiNotFoundError,
} from '../../modules/apiError';

import ComponentInstanceManager from '../../modules/componentInstance/componentInstanceManager';

class ComponentInstanceCreateContext {
    static async call(req) {
        const givenParams = req.body;
        givenParams.idComponent = req.params.idComponent;
        givenParams.idProject = req.params.idProject
        let component = new ComponentInstance(givenParams);

        if (givenParams.parent) {
            const idParent = givenParams.parent;
            const parent = await ComponentInstanceManager.findById(idParent);

            if (!parent) {
                throw new ApiNotFoundError("Resource not found");
            }
            if (parent.idProject !== req.params.idProject) {
                throw new ApiNotFoundError("Resource not found");
            }
            parent.children.push(component._id);
            try {
                await parent.save();
            } catch (e) {
                throw new ApiServerError(e.message);
            }
        }

        try {
            await component.save();
        } catch (err) {
            throw new ApiServerError(err.message);
        }
        return component;
    }

    
}

export default ComponentInstanceCreateContext;