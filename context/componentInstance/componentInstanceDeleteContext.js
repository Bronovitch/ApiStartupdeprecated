

import {
    ApiServerError,
    ApiUnauthorizedError,
    ApiNotFoundError,
} from '../../modules/apiError';



class ComponentInstanceDeleteContext {
    static async call(component, query) {
        await component.remove(query, (err) => {
            if (err) {
                throw new ApiServerError(err.message);
            }
        });

    }
}

export default ComponentInstanceDeleteContext;