import Component from '../../models/component/component';
import {
    ApiServerError,
    ApiUnauthorizedError,
    ApiNotFoundError,
} from '../../modules/apiError';

import ComponentManager from '../../modules/component/componentManager';

class ComponentCreateContext {
    static async call(req) {
        const givenParams = req.body;
        givenParams.project = req.params.idProject;
        let component = new Component(givenParams);

        try {
            await component.save();
        } catch (err) {
            throw new ApiServerError(err.message);
        }
        return component;
    }
}

export default ComponentCreateContext;