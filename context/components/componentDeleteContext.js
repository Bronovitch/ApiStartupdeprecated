import Component from '../../models/component/component';
import {
    ApiServerError,
    ApiUnauthorizedError,
} from '../../modules/apiError';

class ComponentDeleteContext {
    static async call(component, query) {
        await component.remove(query, (err) => {
            if (err) {
                throw new ApiServerError(err.message);
            }
        });

    }
}

export default ComponentDeleteContext;