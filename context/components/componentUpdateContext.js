import Component from '../../models/component/component';
import {
    ApiServerError,
    ApiUnauthorizedError,
    ApiNotFoundError,
} from '../../modules/apiError';

import ComponentManager from '../../modules/component/componentManager';

class ComponentUpdateContext {
    static async call(req) {
        const givenParams = req.body;
        let component;
        try {
            component = await ComponentManager.findById(req.params.idComponent);
        }
        catch (e) {
            throw new ApiServerError(err.message);
        }
        if (!component) {
            throw new ApiNotFoundError("Resource not found");
        }
        try {
            await component.update(givenParams);
        } catch (err) {
            throw new ApiServerError(err.message);
        }
        try {
            component = await ComponentManager.findById(req.params.idComponent);
        }
        catch (e) {
            throw new ApiServerError(err.message);
        }
        return component;
    }
}

export default ComponentUpdateContext;