import Message from '../models/message';
import { ApiServerError } from '../modules/apiError';

class MessageContext {

    static async call(req) {
        let givenParams;
        givenParams = req.body;
        givenParams.user = req.context.me._id;
        const message = new Message(givenParams);
        try {
            await message.save();
        } catch (err) {
            throw new ApiServerError(err.message);
        }
        return message;
    }

}

export default MessageContext;