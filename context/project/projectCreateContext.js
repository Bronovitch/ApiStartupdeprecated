import Project from '../../models/projet/project';

import {
    ApiServerError,
    ApiUnauthorizedError,
} from '../../modules/apiError';

class ProjectCreateContext {
    static async call(req) {
        const givenParams = req.body;
        givenParams.idOwner = req.user._id;
        let project = new Project(givenParams);
        try {
            await project.save();
        } catch (err) {
            throw new ApiServerError(err.message);
        }
        return project;
    }
}

export default ProjectCreateContext;