import User from '../../models/user/user';

import {
  ApiServerError,
  ApiUnauthorizedError,
} from '../../modules/apiError';

class UserContext {

  static async call(req) {
      let givenParams = req;
    if (!givenParams.email || !givenParams.password) {
        throw new ApiUnauthorizedError();
      }

      const user = new User(givenParams);

      try {
        await user.save();
      } catch (err) {
        throw new ApiServerError(err.message);
      }
      return user;
  }
}

export default UserContext;
