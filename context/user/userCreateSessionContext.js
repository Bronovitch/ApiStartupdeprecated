import jwt from 'jsonwebtoken';

import UserManager from '../../modules/user/userManager';
import bcrypt from 'bcrypt-nodejs';

import {
  ApiServerError,
  ApiUnauthorizedError,
} from '../../modules/apiError';

class UserCreateSessionContext {
  static async call(req) {
    let givenParams = req.body;
    let user;

    if (!givenParams.email || !givenParams.password) {
      throw new ApiUnauthorizedError();
    }
    try {
      user = await UserManager.findByEmail(req.body.email);
    } catch (err) {
      throw new ApiUnauthorizedError();
    }
    if (user == null || !bcrypt.compareSync(givenParams.password, user.password)) {
      throw new ApiUnauthorizedError();
    }
    return this.createSession(user, "unknown device name", '1y');
  }

  static async createSession(user, deviceName, expirationTime) {
    const token = jwt.sign(
      { userId: user._id },
      process.env.JWT_SECRET,
      { algorithm: 'HS256', expiresIn: expirationTime, }
    );

    let session = {
      token,
    };
    if (deviceName) {
      session.deviceName = deviceName;
    }
    if (user.sessions instanceof Array) {
      user.sessions.push(session);
    } else {
      user.sessions = [session];
    }

    try {
      await user.save();
    } catch (e) {
      throw new ApiServerError();
    }
    return {user, session};
  }
}

export default UserCreateSessionContext;
