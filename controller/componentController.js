import ComponentCreateContext from '../context/components/componentCreateContext';
import ComponentManager from '../modules/component/componentManager';
import Component from '../models/component/component';

import ComponentDeleteContext from '../context/components/componentDeleteContext';
import ComponentUpdateContext from '../context/components/componentUpdateContext';
import {
    ApiNotFoundError
} from '../modules/apiError';


import ComponentSerializer from '../serializer/componentSerializer';

class ComponentController {

    static async create(req, res, next) {
        let doc;

        try {
            doc = await ComponentCreateContext.call(req);
        }
        catch (e) {
            return next(e);
        }
        return res.json({
            data: [ComponentSerializer(doc)],
            includes: [],
        });
    }

    static async update(req, res, next) {
        let comp;

        try {
           comp =  await ComponentUpdateContext.call(req);
        } catch (e) {
            return next(new ApiNotFoundError("Resource not found"));
        }
        return res.json({
            data: [ComponentSerializer(comp)],
            includes: [],
        });
    }

    static async delete(req, res, next) {
        let component = await ComponentManager.findById(req.params.idComponent);

        if (!component) {
            return next(new ApiNotFoundError("Resource not found"));
        }

        if (component.project !== req.params.idProject) {
            return next(new ApiNotFoundError("Component not belong to this project."));
        }
        try {
            await ComponentDeleteContext.call(component, { _id: component._id });
        } catch (err) {
            return next(err);
        }
        return res.json({
            data: [],
            includes: [],
        });
    }


    static async find(req, res, next) {
        let components;

        if (!(components = await ComponentManager.findAll())) {
            return next(new ApiNotFoundError("Resource not found"));
        }
        return res.json({
            data: component.map((item) => ComponentSerializer(item)),
            includes: [],
        });
    }

    static async getMyComponent(req, res, next) {
        let component;

        if (!(component = await ComponentManager.findByOwner(req.user._id))) {
            return next(new ApiNotFoundError("Resource not found"));
        }
        return res.json({
            data: component.map((item) => ComponentSerializer(item)),
            includes: [],
        });
    }

    static async getProjectComponent(req, res, next) {
        const project = req.params.idProject;
        let component;

        if (!(component = await ComponentManager.findByProject(project))) {
            return next(new ApiNotFoundError("Resource not found"));
        }
        return res.json({
            data: [component.map((item) => ComponentSerializer(item))],
            includes: [],
        });
    }
}

export default ComponentController
