import {
    ApiNotFoundError,
    ApiUnauthorizedError,
} from '../modules/apiError';

import ComponentInstanceManager from '../modules/componentInstance/componentInstanceManager';
import ComponentInstanceContext from '../context/componentInstance/componentInstanceContext';
import ComponentInstanceDeleteContext from '../context/componentInstance/componentInstanceDeleteContext';

import ComponentInstanceSerializer from '../serializer/componentInstanceSerializer';

class ComponentInstanceController {

    static async create(req, res, next) {
        let doc;
        
        try {
            doc = await ComponentInstanceContext.call(req);
        }
        catch (e) {
            return next(e);
        }
        return res.json({
            data: [ComponentInstanceSerializer(doc)],
            includes: [],
        });
    }

    static async findAll(req, res, next) {
        let components;

        if (!(components = await ComponentInstanceManager.findByProject(req.params.idProject))) {
            return next(new ApiNotFoundError("Resource not found"));
        }

        return res.json({
            data: components.map((item) => ComponentInstanceSerializer(item)),
            includes: [],
        });
    }

    static async delete(req, res, next) {
        let instance = await ComponentInstanceManager.findById(req.params.idInstance);
    
        if (!instance) {
            return next(new ApiNotFoundError("Resource not found"));
        }
        if (instance.children.length !== 0)
        {
            return next(new ApiUnauthorizedError("Can't delete Instance with children"));
        }
        try {
            await ComponentInstanceDeleteContext.call(instance, { _id: instance._id });
        } catch (err) {
            return next(err);
        }
        return res.json({
            data: [],
            includes: [],
        });
    }
}

export default ComponentInstanceController
