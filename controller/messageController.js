import MessageManager from '../modules/messageManager';
import MessageContext from '../context/messageContext';
class MessageController {

  static async find(req, res, next) {
    let message;

    try {
      message = await MessageManager.findAll();
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: message, //use Serializer here
      includes: [],
    });
  }

  static async findById(req, res, next) {
    let message;

    try {
      message = await MessageManager.findById(req.params.id);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: message,
      includes: [],
    });
  }

  static async create(req, res, next) {
    let message;

    try {
      message = await MessageContext.call(req);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: message,
      includes: [],
    });
  }
}

export default MessageController
