import ProjectCreateContext from '../context/project/projectCreateContext';
import ProjectManager from '../modules/project/projectManager';
import ProjectSerializer from '../serializer/projectSerializer';

class ProjectController {

  static async create(req, res, next) {
    let project;
    
    try {
        project = await ProjectCreateContext.call(req);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: ProjectSerializer(project),
      includes: [],
    });
  }

  static async getMyProject(req, res, next) {
    let project;

    if (!(project = await ProjectManager.findByOwner(req.user._id))) {
        return next(new ApiNotFoundError("Resource not found"));
    }
    return res.json({
        data: project.map((item) => ProjectSerializer(item)),
        includes: [],
    });
}
}

export default ProjectController
