import UserCreateSessionContext from '../context/user/userCreateSessionContext';
import SessionSerializer from '../serializer/sessionSerializer';

class SessionController {

  static async create(req, res, next) {
    let user;

    try {
      user = await UserCreateSessionContext.call(req);
    } catch (err) {
      return next(err);
    }
    return res.json({
        data: [SessionSerializer(user.user, user.session)],
     // data: [user],
      includes: [],
    });
  }
}

export default SessionController