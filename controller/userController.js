import UserManager from '../modules/user/userManager';
import UserContext from '../context/user/userContext';
import UserCreateSessionContext from '../context/user/userCreateSessionContext';
import UserSerializer from '../serializer/userSerializer';
import SessionSerializer from '../serializer/sessionSerializer';
import {
  ApiForbiddenError
} from '../modules/apiError';
import { Session } from 'inspector';


class UserController {

  static async test(req, res, next) {
    return res.json({
      "data": [
        {
          "type": "user",
          "id": "5c90e9231365e132007f693a",
          "username": "bronozdzditch",
          "email": "a@adsdsdzdz.fr"
        },
        {
          "type": "userz",
          "id": "5c90e9231365e132007f693ZZZa",
          "username": "bronozdzditch",
          "email": "a@adsdsdzdz.fr"
        }
      ],
      "includes": [
        {
          "type": "user",
          "id": "5c90e9231365e132007f693a",
          "username": "bronozdzditch",
          "email": "a@adsdsdzdz.fr"
        },
        {
          "type": "user",
          "id": "5c90e9231365e132007f693ZZZa",
          "username": "bronozdzditch",
          "email": "a@adsdsdzdz.fr"
        }
      ]
    });
  }

  static async create(req, res, next) {
    let user;

    if ((user = await UserManager.findByEmail(req.body.email)) != null) {
      return next(new ApiForbiddenError('User with that email address already exist'));
    }
    if ((user = await UserManager.findByUsername(req.body.username)) != null) {
      return next(new ApiForbiddenError('User with that nickname already exist'));
    }
    if (req.body.password.length < 3 || req.body.password.length > 22) {
      return next(new ApiForbiddenError('Password should be longer than 8 and shorter than 22 characters'));
    }
    try {
      user = await UserContext.call(req.body);
      await UserCreateSessionContext.createSession(user, "unknown device name", '1y');
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: [UserSerializer(user)],
      includes: [SessionSerializer(user)],
    });

  }

  static async find(req, res, next) {
    let user;

    try {
      user = await UserManager.findAll();
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: user, //use Serializer here
      includes: [],
    });
  }

  static async findById(req, res, next) {
    let user;

    try {
      user = await UserManager.findById(req.params.id);
    } catch (err) {
      return next(err);
    }
    return res.json({
      data: user, //use Serializer here
      includes: [],
    });
  }

}

export default UserController
