import jwt from 'express-jwt';
import UserManager from '../modules/user/userManager';

import {
    ApiUnauthorizedError,
    ApiForbiddenError,
} from '../modules/apiError';

class ApiAuthentication {
    static validJwt() {
        const jwtValidation = jwt({
            secret: process.env.JWT_SECRET,
            requestProperty: 'jwtToken',
            getToken: function fromHeaderOrQuerystring(req) {
                if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
                    return req.headers.authorization.split(' ')[1];
                } else if (req.query && req.query.accessToken) {
                    return req.query.accessToken;
                }
                return null;
            }
        });
        return jwtValidation;
    }

    static async retrieveUser(req, res, next) {
        let user = {};
        try {
            user = await UserManager.findById(req.jwtToken.userId);
        } catch (err) {

            return next(new ApiUnauthorizedError(err.message));
        }

        if (user == null) {
            return next(new ApiForbiddenError('Cannot find user token'));
        }
        req.user = user;
        next();
    }
}

export default ApiAuthentication;