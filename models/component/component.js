import mongoose from 'mongoose';
import PropSchema from './propschema';

const componentSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: "unknown"
    },
    type: {
        type: String,
        default: "simple"
    },
    folder:{
        type:String,
        default:null
    },
    props: {
        type: [PropSchema],
        default: []
    },
    state: {
        type: [PropSchema],
        default: []
    },
    project: {
        type: String,
        required: true,
    }
});

componentSchema.pre('remove', function (next) {
    this.model('ComponentInstance').deleteMany({ idComponent: this._id }, next);
});

const Component = mongoose.model('Component', componentSchema);

export default Component;