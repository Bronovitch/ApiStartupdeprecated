import mongoose from 'mongoose';

const PropSchema = mongoose.Schema({
    type: {
        type: String,
        required: true,
        default: "String"
    },
    name: {
        type: String,
        required: true,
        default: "unknown"
    }
});

export default PropSchema;
