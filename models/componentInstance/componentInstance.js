import mongoose from 'mongoose';
import {
    ApiServerError,
    ApiUnauthorizedError,
    ApiNotFoundError,
} from '../../modules/apiError';

const ComponentInstanceSchema = new mongoose.Schema({
    idComponent: {
        type: String,
        required: true
    },
    idProject: {
        type: String,
        required: true
    },
    folder:{
        type: String,
        default: null
    },
    parent: {
        type: String,
        default: null
    },
    children: {
        type: [String],
        default: []
    }
});

ComponentInstanceSchema.pre('remove', function (next) {

    this.model('ComponentInstance').updateMany({},
        { $pull: { children: { $in: this._id } } },
        { multi: true },
        next);
});

const ComponentInstance = mongoose.model('ComponentInstance', ComponentInstanceSchema);

export default ComponentInstance;