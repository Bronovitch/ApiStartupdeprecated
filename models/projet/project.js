import mongoose from 'mongoose';

const ProjectSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: "unknown"
    },
    idOwner: {
        type:String,
        required:true,
    },
    users:{
        type:[String],
        default:[]
    },
});

ProjectSchema.pre('remove', function (next) {
    this.model('Component').deleteMany({ project: this._id }, next);
});

const Project = mongoose.model('Project', ProjectSchema);

export default Project;