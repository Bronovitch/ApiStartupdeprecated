import mongoose from 'mongoose';
import moment from 'moment';
import bcrypt from 'bcrypt-nodejs';

import UserSessionSchema from './userSessionSchema';

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        unique: true,
    },
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true,
    },
    create_date: {
        type: Number,
        default: moment().unix(1318874398806),
    },
    update_date: {
        type: Number,
        default: moment().unix(1318874398806),
    },
    sessions: {
        type: [UserSessionSchema],
        default: {}
    }
});

userSchema.statics.findByLogin = async function (login) {
    let user = await this.findOne({
        username: login,
    });
    if (!user) {
        user = await this.findOne({ email: login });
    }
    return user;
};

userSchema.pre('save', async function (next) {
    var user = this;
    var salt;
    if (!user.isModified('password'))
        return next();
    try {
        user.password = await bcrypt.hashSync(user.password);
        next();
    } catch (err) {
        return next(err);
    }
});


userSchema.pre('remove', function (next) {
    this.model('Message').deleteMany({ user: this._id }, next);
});
const User = mongoose.model('User', userSchema);

export default User;