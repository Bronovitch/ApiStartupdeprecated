import mongoose from 'mongoose';

const UserSessionSchema = mongoose.Schema({
  token: {
          type: String,
          required: true,
          default:"unknows"
  },
  deviceName: {
          type: String,
          minlength: 1,
          maxlength: 64,
          default: 'Unknown',
          required: true,
  },
});

export default UserSessionSchema;
