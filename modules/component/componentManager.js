import Component from '../../models/component/component';

class ComponentManager {
    
    static async findAll() {
        return (await Component.find());
    }
    static async findByProject(req) {
        return (await Component.find({ project: req }));
    }

    static async findById(req) {
        return (await Component.findById(req));
    }
}

export default ComponentManager;