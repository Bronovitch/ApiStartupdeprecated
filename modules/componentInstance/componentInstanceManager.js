import ComponentInstance from '../../models/componentInstance/componentInstance';

class ComponentInstanceManager {

    static async findAll() {
        return (await ComponentInstance.find());
    }
    static async findByProject(req) {
        return (await ComponentInstance.find({ idProject: req }));
    }
    static async findById(req) {
        return (await ComponentInstance.findById(req));
    }
}

export default ComponentInstanceManager;