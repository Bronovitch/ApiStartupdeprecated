import Message from '../models/message';
class MessageManager {
  
    static async findAll() {
      return (await Message.find());
    }
    
    static async findById(messageId) {
      return (await Message.findById(messageId));
    }
  }
  
  export default MessageManager;
  