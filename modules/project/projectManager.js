import Project from '../../models/projet/project';

class ProjectManager {
    static async findAll() {
        return (await Project.find());
    }
    static async findByOwner(req) {
        return (await Project.find({idOwner: req}));
      }   
}

export default ProjectManager;