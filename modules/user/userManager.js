import User from '../../models/user/user';

class UserManager {
  static async findAll() {
    return (await User.find());
  }
  static async findById(u) {
    return (await User.findById(u));
  }
  static async findByEmail(req) {
    return (await User.findOne({ email: req }));
  }
  static async findByUsername(req) {
    return (await User.findOne({ username: req }))
  }
}

export default UserManager;