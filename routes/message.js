import uuidv4 from 'uuid/v4';
import { Router } from 'express';
import MessageController from '../controller/messageController'
const router = Router();


router.route('/build')
  .get((req, res) => {
    //https://stackoverflow.com/questions/36601121/how-to-execute-a-bat-file-from-node-js-passing-some-parameters
    /** TEST PURPOSE */
    var child_process = require('child_process');
    var bat = require.resolve('../scripts/echo.bat');
    child_process.exec(bat, function (error, stdout, stderr) {
      console.log(stdout);
      return res.send(stdout)
    });
  })
router.route('/')
  .get(MessageController.find);

router.route('/:id')
  .get(MessageController.findById);

router.route('/')
  .post(MessageController.create)

router.delete('/:messageId', async (req, res) => {
  const message = await req.context.models.Message.findById(
    req.params.messageId,
  );

  let result = null;
  if (message) {
    result = await message.remove();
  }

  return res.send(result);
});

export default router;