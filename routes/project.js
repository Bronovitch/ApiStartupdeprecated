import { Router } from 'express';
import ApiAuthentication from '../middlewares/apiAuthentication';
import ProjectController from '../controller/projectController';
import ComponentController from '../controller/componentController'
import ComponentInstanceController from '../controller/componentInstanceController';
import FolderController from '../controller/folderController';
const router = Router();

router.route('/')
    .post(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ProjectController.create)
    .get(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ProjectController.getMyProject);

router.route('/:idProject/component')
    .get(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentController.getProjectComponent)
    .post(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentController.create)

router.route('/:idProject/component/:idComponent')
    .delete(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentController.delete)
    .patch(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentController.update)

router.route('/:idProject/component/:idComponent/instance')
    .post(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentInstanceController.create)

router.route('/:idProject/instance')
    .get(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentInstanceController.findAll)

router.route('/:idProject/instance/:idInstance')
    .delete(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, ComponentInstanceController.delete)

router.route('/:idProject/folder')
    .post(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, FolderController.create)
    .get(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, FolderController.findByProjectId)
export default router;