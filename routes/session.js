import { Router } from 'express';
import SessionController from '../controller/sessionController';

const router = Router();

router.route('/')
.post(SessionController.create);

export default router;