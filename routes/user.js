import { Router } from 'express';
import UserController from '../controller/userController';
import ApiAuthentication from '../middlewares/apiAuthentication';

const router = Router();

router.route('/')
  .get(ApiAuthentication.validJwt(), ApiAuthentication.retrieveUser, UserController.find);
/*
router.route('/:id')
  .get(UserController.findById)*/
  
router.route('/create')
  .post(UserController.create)

  router.route('/test')
  .get(UserController.test)
export default router;